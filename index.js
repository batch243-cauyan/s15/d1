

let myVariable = "Ada Lovelace";



let outerVariable ="hello from the other side";

{
    let innerVariable ="hello from the block";
}

console.log(outerVariable);



let province = "Quezon";
let country ="Philippines";

let greeting = 'I live in the '+country;
console.log(greeting);

// The escape characters (\) in strings in combination with other characters can produce different effects.

// "\n" refers to creating a new line between text

let mailAddress ="Metro Manila\nPhilippines";

console.log(mailAddress);

let message = 'John\'s employees went home early.'

let count="26";
let headcount = 26;

console.log(count);
console.log(headcount);

// Decimal Numbers/ Fractions
let grade = 98.7;
console.log(grade);


// exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// type coercion
console.log("John's grade last quarter is " + grade);
console.log(count + headcount);

let isMarried = false;
let isGoodConduct = true;

console.log(isMarried);
console.log(isGoodConduct);

console.log("isMarried: " + isMarried);


// Arrays
// Arrays are a special kind of data type that's used to store multiple values.

// Arrays can store different data types but is normally use to stode similar data types.

    // Similar data types
        // Syntax
        // let/const arrayName = [elementA, elementB, elementC . . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades[1]);
console.log(grades);


let person = {
    fullName: "Juan Dela Cruz",
    age:35,
    isMarried:false,
    contact: ["09171234567" ,"8123 4567"],
    address:{
        houseNumber: '345',
        city: "Manila"
    }
}


let heading = document.createElement('h1');
heading.innerText = "WELCOME TO THIS PAGE";
document.body.appendChild(heading);
console.log(message);

console.log(person);



/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */

            // Null is ued to intentionally express the absence of a value in a variable/initialization.

            // Null simply means that a data type was assigned to a variable but it does not hold any value/amount
            // or is nullified.


            let spouse = null;
            console.log(spouse);

            // Undefined it represents a state of a variable that has been declared but without value.
            let fullName;
            console.log(fullName);

            // undefined vs Null
            // The difference between undefined and nulls is that for undefined, a variable was created but not
            // provided a value.

            // null means that a variable was created and was assigned a value that does not hold any value/amount.
            
